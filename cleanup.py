import argparse
import logging
from urllib import request

from repomanager.repoman import GitLab

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Migrate OCA repositories to Flectra Community')
    parser.add_argument('--src', default='./OCA', help='Path to OCA root')
    parser.add_argument('--dest', default='./FC', help='Path to Flectra root')
    parser.add_argument('--update-key', default=False, action='store_true', help='Update gitlab deploy keys')
    parser.add_argument('--no-create', default=False, action='store_true', help='Do not create repositories at gitlab')
    parser.add_argument('--no-commit', default=False, action='store_true', help='Do not commit changes')
    parser.add_argument('--no-push', default=False, action='store_true', help='Do not push changes')
    parser.add_argument('--debug', default=False, action='store_true', help='Loglevel: DEBUG')
    parser.add_argument('--single-repo', default=[], action='append', help='Repository name e.g. partner-contact')
    parser.add_argument('--private-token', help='Gitlab token')
    parser.add_argument('--private-token-delete', help='Gitlab token')
    args = parser.parse_args()

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%a, %d %b %Y %H:%M:%S'
    )
    _logger = logging.getLogger(__name__)
    _logger.info('Starting module migration')

    gl = GitLab(args.dest, args.private_token)
    fc_repos = gl.repositories

    for repo, data in fc_repos.items():
        if data['has_master_branch']:
            _logger.warning('%s has master branch - skipping', repo)
            continue
        if not data['has_upstream_branch']:
            _logger.warning('%s has no upstream branch - skipping', repo)
            continue

        _logger.info('%s selected for remove', repo)

        req = request.Request(
            url='https://gitlab.com/api/v4/projects/%s' % data['id'],
            method='DELETE',
            headers={
                'Private-Token': args.private_token_delete,
            }
        )
        response = request.urlopen(req)
        _logger.info('Deletion with return code %s', response.code)
