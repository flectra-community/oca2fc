# OCA to Flectra Community Migrator

This Repository is made to migrate OCA modules located 
at github.com and available for Odoo 14.0 to Flectra 2.0.

## Process

Based on a gitlab-ci job all OCA repositories will be cloned and migrated to flectra.
If there is not yet a corresponding flectra repository this will be created and configured with API calls.
Any changes will be committed and pushed in to a special branch called *20-upstream* of flectra repository on gitlab.

While migration a gitlab-ci file is also generated for automatic testing of migrated modules.
Depending repositories are also integrated to make shure that tests will not fail because of missing dependencies. 

To avoid too much gitlab jobs running at the same time the script will stop migration after 20 changed repositories.

Automation is made with a pipeline scheduler which is running every 2 hours to search for changes.
When all modules are migrated the first time it would be a good idea to raise the period to not exceed 
the pipeline limit.


## Requirements

- Python 3.6 or later
- gitpython (installed with requirements.txt)
- networkx (installed with requirements.txt)
- requests (installed with requirements.txt)

If you would like to generate dependency graphs, you have to install 
the following packages:

- matplotlib
- pygraphviz
- pydot

## TODO

- The script itself is working nice but there will certainly occur some unexpected behaviors because of usage of REGEX
and other replace statements.
- Extension to use with multiple odoo/flectra releases
