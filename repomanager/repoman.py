import json
import logging
from collections import OrderedDict
from os.path import join as oj
from urllib import request
from urllib.error import HTTPError

import git
from git import Repo, Actor

_logger = logging.getLogger(__name__)


class GitHub(object):
    _base_path = False
    _repos = {}

    _use_other_repos = {
        'l10n-italy': 'https://github.com/Odoo-Italia-Associazione/l10n-italy.git',
    }

    _additional_repos = {
        'odoo-product-configurator': 'git@github.com:pledra/odoo-product-configurator.git',
        'muk_base': 'git@github.com:muk-it/muk_base.git',
        'muk_web': 'git@github.com:muk-it/muk_web.git',
        'muk_dms': 'git@github.com:muk-it/muk_dms.git',
        'muk_website': 'git@github.com:muk-it/muk_website.git',
        'muk_quality': 'git@github.com:muk-it/muk_quality.git',
        'muk_misc': 'git@github.com:muk-it/muk_misc.git',
    }

    _ignore_repos = [
        'OpenUpgrade',
        'contribute-md-template',
        'OCB',
        'odoo-community.org',
        'ansible-odoo',
        'apps-store',
    ]

    def __init__(self, base_path, selected_repos=False, no_oca=False):
        self._base_path = base_path
        self._selected_repos = selected_repos
        self._no_oca = no_oca
        if not self._no_oca:
            self._read_repos()
        for name, url in self._additional_repos.items():
            if selected_repos and name not in selected_repos:
                continue
            self._repos[name] = url


    @property
    def repositories(self):
        return self._repos

    def _read_repos(self, ):
        if self._selected_repos:
            for single_repo in self._selected_repos:
                if single_repo in self._additional_repos:
                    continue
                _logger.info('Asking github for OCA repository %s' % single_repo)
                response = request.urlopen('https://api.github.com/repos/OCA/%s' % single_repo)
                repo = json.loads(response.read())
                if repo:
                    if repo['name'] in self._use_other_repos:
                        self._repos[repo['name']] = self._use_other_repos[repo['name']]
                    else:
                        self._repos[repo['name']] = repo['git_url']
                _logger.info('Given repository of OCA found')
            return

        _logger.info('Asking github for OCA repositories')
        has_repos = True
        page = 1
        while has_repos:
            response = request.urlopen('https://api.github.com/orgs/OCA/repos?page=%s' % page)
            repos = json.loads(response.read())
            if not repos:
                has_repos = False
                continue

            for repo in repos:
                if repo['name'] in self._ignore_repos:
                    continue
                if repo['name'] in self._use_other_repos:
                    self._repos[repo['name']] = self._use_other_repos[repo['name']]
                else:
                    self._repos[repo['name']] = repo['git_url']
            page += 1

        _logger.info('%s repositories of OCA found', len(self._repos))
        return

    def clone(self, name, url):
        _logger.info('Cloning OCA repository %s', name)
        try:
            repo = Repo.clone_from(url, oj(self._base_path, name), depth=1, branch='14.0')
        except:
            return False
        return repo


class GitLab(object):
    _base_path = False
    _repos = {}

    _ignore_repos = [
        'oca2fc',
    ]

    _rename_repos = {
        'odoo-product-configurator': 'product-configurator',
    }

    _fc_project_settings = {
        "tag_list": [],
        "visibility": "public",
        "resolve_outdated_diff_discussions": False,
        "container_registry_enabled": False,
        "issues_enabled": True,
        "merge_requests_enabled": True,
        "wiki_enabled": False,
        "jobs_enabled": True,
        "snippets_enabled": False,
        "shared_runners_enabled": True,
        "lfs_enabled": False,
        "public_jobs": True,
        "only_allow_merge_if_pipeline_succeeds": False,
        "request_access_enabled": False,
        "only_allow_merge_if_all_discussions_are_resolved": False,
        "merge_method": "merge",
        "approvals_before_merge": 0,
        "mirror": False,
    }

    _hook_data = {
        'url': 'https://flectra-community.org/gitlab/hook',
        'push_events': True,
        'tag_push_events': True,
        'merge_requests_events': True,
        'repository_update_events': False,
        'enable_ssl_verification': True,
        'issues_events': False,
        'confidential_issues_events': False,
        'note_events': False,
        'confidential_note_events': False,
        'pipeline_events': True,
        'wiki_page_events': True,
        'job_events': True,
        'token': 'FlectraCommunityGitLabUpdate',
    }

    _protected_branch_data = [
        {
            'name': '1.0',
            'push_access_level': 0,
            'merge_access_level': 40,
        },
        {
            'name': 'oca-fixed',
            'push_access_level': 40,
            'merge_access_level': 30,
        },
        {
            'name': 'oca-upstream',
            'push_access_level': 40,
            'merge_access_level': 0,
        },
        {
            'name': '2.0',
            'push_access_level': 0,
            'merge_access_level': 40,
        },
        {
            'name': '20-fixed',
            'push_access_level': 40,
            'merge_access_level': 30,
        },
        {
            'name': '20-upstream',
            'push_access_level': 40,
            'merge_access_level': 0,
        },
    ]

    _public_deploy_key = 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCshGCv+V6fuRSVJF5I2f7mKa/fjfiCXH+qvSwbAFRjF3WvkClnimdc8EtD581UhLydF3LuRGtPrr1v9dFwzDjiOuCJNojxtwINUNbsUfqkGwD2u/kjIwTn3jSHA/fi/hqxQ8qp3ua1yFhHIgl0hm0GIm8pM9wHcZa5wCvSosuwjhUV5cwIswXa6bg5jjK4rmQsgTuFNRMi+WBHxW2MBxDh98s/wzp/JOSctt3iomwYAAfZ2o4Z0WLBC9r2XcsG+Ufu3ZVDeFPlLMG3J344qEg9fHCtr/kHEICvqhTJPgn3t+4bZFyu12LA6wAyvxH7tcbcanZabR5HFoMcGtMAY/mZOtx7dEU+RQHfG5tm05+oEgFjt66gamWvV0V9irTxUkLpk9acQOA4C/4pCJHxjGgW6CbAdG1KCmg0ONtIEL6lDCiCz9KA44jkmLIYXXq3Ky4SrjSCT+wBgmKkMcc4u8wx9p9ra2XD8Zn7YdVj+UoN+8i3Wqez0c1cFQYa7fQvC7l0bw3iyNRa/Eq1uhypuSyw0QXm0pb0Kf5Tk+JNMt0B7qpn5JQObXMB3wLL0hYvA6X3IDZFtGuzikoDYn0JixrKQDP1DuIXnwR01kQqNccAQXCR+e/NL+1Vl45vO95VC3Ubr/rqyjgXQrxLmwOTRvxwWe1XcB/XORCjEp8F0+rigw== info@flectra-community.org'

    def __init__(self, base_path, private_token, update_project=True, selected_repos=False, read_hooks=True, read_branches=True):
        self._base_path = base_path
        self._private_token = private_token
        self._update_project = update_project
        self._selected_repos = selected_repos and [self.get_new_name(r) for r in selected_repos] or False
        self._read_hooks = read_hooks
        self._read_branches = read_branches
        self._repos = {}
        self._read_repos()

    @property
    def repositories(self):
        return self._repos

    def _accept_merge_request(self, repo, mr_id):
        data = {
            'merge_when_pipeline_succeeds': True,
        }
        req = request.Request(
                url='https://gitlab.com/api/v4/projects/%s/merge_requests/%s/merge' % (repo['id'], mr_id),
                data=json.dumps(data).encode('utf8'),
                method='PUT',
                headers={
                    'Private-Token': self._private_token,
                    'Content-Type': 'application/json'
                }
        )
        try:
            response = request.urlopen(req)
            if response.code != 201:
                _logger.error('Error while create new MR for project %s with code %s', repo['name'], response.code)
                return False
            else:
                _logger.debug('Merge request of project %s with id %s accepted', repo['name'], mr_id)
                return True
        except HTTPError as err:
            result = err.fp.read()
            _logger.error(result)

    def create_merge_request(self, repo):
        data = {
            'source_branch': '20-fixed',
            'target_branch': '2.0',
            'title': 'Automatic MR created by OCA2FC Migrator',
        }
        req = request.Request(
                url='https://gitlab.com/api/v4/projects/%s/merge_requests' % repo['id'],
                data=json.dumps(data).encode('utf8'),
                method='POST',
                headers={
                    'Private-Token': self._private_token,
                    'Content-Type': 'application/json'
                }
        )
        try:
            response = request.urlopen(req)
            if response.code != 201:
                _logger.error('Error while create new MR for project %s with code %s', repo['name'], response.code)
                return False
            result = json.loads(response.read())
            mr_id = result.get('iid')
            if not mr_id:
                _logger.error('Merge request not created for project %s', repo['name'])
                return False

            _logger.debug('Merge request with id %s for project %s created', mr_id, repo['name'])
            return self._accept_merge_request(repo, mr_id)

        except HTTPError as err:
            result = err.fp.read()
            _logger.error(result)

    def update_project_settings(self):
        if not self._update_project:
            return

        for repo in self._repos.values():
            self._update_project_settings(repo)
            self._update_project_branch_settings(repo)
            self._update_project_hook(repo)

    def _read_repos(self):
        self._repos = {}
        _logger.info('Asking gitlab for Flectra Community repositories')
        _logger.debug('Reading gitlab group informations')
        response = request.urlopen('https://gitlab.com/api/v4/groups/flectra-community')
        group = json.loads(response.read())
        repos = group['projects']
        if self._selected_repos:
            sorted_repos = sorted([r for r in repos if r['path'] in self._selected_repos], key=lambda r: r['path'])
        else:
            sorted_repos = sorted(repos, key=lambda r: r['path'])
        for repo in [r for r in sorted_repos if r['path'] not in self._ignore_repos]:
            self._repos[repo['path']] = repo

        if self._read_hooks:
            self._read_repo_hooks()
        if self._read_branches:
            self._read_repo_branches()

    def _read_repo_hooks(self):
        for name, repo in self._repos.items():
            req = request.Request(
                    url='https://gitlab.com/api/v4/projects/%s/hooks' % repo['id'],
                    method='GET',
                    headers={
                        'Private-Token': self._private_token,
                        'Content-Type': 'application/json'
                    }
            )
            _logger.debug('Reading gitlab hooks of %s', name)
            response = request.urlopen(req)
            repo['hooks'] = json.loads(response.read())

    def _read_repo_branches(self):
        for name, repo in self._repos.items():
            _logger.debug('Reading gitlab branches of %s', name)
            response = request.urlopen('https://gitlab.com/api/v4/projects/%s/repository/branches' % repo['id'])
            repo['branches'] = json.loads(response.read())
            req = request.Request(
                    url='https://gitlab.com/api/v4/projects/%s/protected_branches' % repo['id'],
                    method='GET',
                    headers={
                        'Private-Token': self._private_token,
                        'Content-Type': 'application/json'
                    }
            )
            response = request.urlopen(req)
            repo['protected_branches'] = json.loads(response.read())

            repo['has_upstream_branch'] = False
            repo['has_master_branch'] = False
            repo['has_fix_branch'] = False
            for branch in repo['branches']:
                if branch['name'] == '20-upstream':
                    repo['has_upstream_branch'] = True
                if branch['name'] == '2.0':
                    repo['has_master_branch'] = True
                if branch['name'] == '20-fixed':
                    repo['has_fix_branch'] = True

    def _update_project_hook(self, repo):
        method = False
        if repo['hooks']:
            current_hook = repo['hooks'][0]
            needs_update = False
            for hd in self._hook_data:
                if hd in current_hook and current_hook[hd] != self._hook_data[hd]:
                    needs_update = True
                    break
            if needs_update:
                method = 'PUT'
                url = 'https://gitlab.com/api/v4/projects/%s/hooks/%s' % (repo['id'], current_hook['id'])
        else:
            method = 'POST'
            url = 'https://gitlab.com/api/v4/projects/%s/hooks' % repo['id']

        if method:
            data_str = json.dumps(self._hook_data).encode('utf8')
            req = request.Request(
                    url=url,
                    method=method,
                    data=data_str,
                    headers={
                        'Private-Token': self._private_token,
                        'Content-Type': 'application/json'
                    }
            )
            _logger.debug('Create or update gitlab hook of %s', repo['path'])
            try:
                response = request.urlopen(req)
                if response.code not in [200, 201]:
                    result = response.read()
                    _logger.warning(result)
            except HTTPError as err:
                result = err.fp.read()
                _logger.error(result)

    def _update_project_branch_settings(self, repo):
        for to_protect in self._protected_branch_data:
            needs_update = False
            existing = [r for r in repo['protected_branches'] if r['name'] == to_protect['name']]
            if existing:
                existing = existing[0]
                if existing['push_access_levels'][0]['access_level'] != to_protect['push_access_level'] or \
                        existing['merge_access_levels'][0]['access_level'] != to_protect['merge_access_level']:
                    needs_update = True
                    req = request.Request(
                            url='https://gitlab.com/api/v4/projects/%s/protected_branches/%s' % (
                                repo['id'], to_protect['name']),
                            method='DELETE',
                            headers={
                                'Private-Token': self._private_token,
                                'Content-Type': 'application/json'
                            }
                    )
                    try:
                        response = request.urlopen(req)
                        if response.code != 204:
                            result = response.read()
                            _logger.warning(result)
                    except HTTPError as err:
                        result = err.fp.read()
                        _logger.error(result)
            else:
                needs_update = True

            if needs_update:
                data_str = json.dumps(to_protect).encode('utf8')
                req = request.Request(
                        url='https://gitlab.com/api/v4/projects/%s/protected_branches' % repo['id'],
                        method='POST',
                        data=data_str,
                        headers={
                            'Private-Token': self._private_token,
                            'Content-Type': 'application/json'
                        }
                )
                _logger.debug('Create or update gitlab branch protection of %s', repo['path'])
                try:
                    response = request.urlopen(req)
                    if response.code != 201:
                        result = response.read()
                        _logger.warning(result)
                except HTTPError as err:
                    result = err.fp.read()
                    _logger.error(result)

    def _update_project_settings(self, repo):
        _logger.debug('Checking gitlab project settings of %s', repo['path'])
        data = {}
        for key, val in self._fc_project_settings.items():
            if repo[key] != val:
                data[key] = val

        if data:
            _logger.debug('Updating gitlab project settings of %s', repo['path'])
            to_update_str = json.dumps(data).encode('utf8')
            req = request.Request(
                    url='https://gitlab.com/api/v4/projects/%s' % repo['id'],
                    data=to_update_str,
                    method='PUT',
                    headers={
                        'Private-Token': self._private_token,
                        'Content-Type': 'application/json'
                    }
            )
            try:
                response = request.urlopen(req)
                if response.code != 200:
                    result = response.read()
                    _logger.warning(result)
            except HTTPError as err:
                result = err.fp.read()
                _logger.error(result)

    def get_new_name(self, name):
        return self._rename_repos.get(name, name)

    def get_repository(self, name):
        name = self._rename_repos.get(name, name)
        if name not in self._repos:
            return False
        return self._repos[name]

    def clone(self, name):
        name = self._rename_repos.get(name, name)
        _logger.info('Cloning Flectra Community repository %s', name)
        repo = self.get_repository(name)

        repo_path = oj(self._base_path, name)
        if repo['has_upstream_branch']:
            repo = Repo.clone_from(repo['ssh_url_to_repo'], repo_path, branch='20-upstream')
        else:
            repo = Repo.clone_from(repo['ssh_url_to_repo'], repo_path, depth=1)
            repo.git.checkout(b='20-upstream')
        return repo

    def create(self, name):
        name = self._rename_repos.get(name, name)
        data = self._fc_project_settings
        data.update({
            "name": name,
            "namespace_id": 3617678,
        })
        data = json.dumps(data).encode('utf8')

        req = request.Request(
                url='https://gitlab.com/api/v4/projects/',
                data=data,
                method='POST',
                headers={
                    'Private-Token': self._private_token,
                    'Content-Type': 'application/json'
                }
        )
        try:
            response = request.urlopen(req)
            if response.code != 201:
                _logger.error('Error while create new project %s with code %s', name, response.code)
                return False

            repo = json.loads(response.read())
            self._check_and_update_deploy_keys(repo)
            self._read_repos()
            return True

        except HTTPError as err:
            result = err.fp.read()
            _logger.error(result)
            return False

    def _check_and_update_deploy_keys(self, repo):
        _logger.debug('Reading gitlab project deploy keys of %s', repo['path'])
        req = request.Request(
                url='https://gitlab.com/api/v4/projects/%s/deploy_keys/' % repo['id'],
                headers={
                    'Private-Token': self._private_token,
                }
        )
        response = request.urlopen(req)
        keys = json.loads(response.read())

        fc_key = False
        for key in keys:
            if key['title'] == 'oca2fc':
                fc_key = key
                break

        data = {
            'title': 'oca2fc',
            'can_push': True,
            'key': self._public_deploy_key,
        }
        url = 'https://gitlab.com/api/v4/projects/%s/deploy_keys' % repo['id']

        method = False
        if fc_key:
            if fc_key['key'] != self._public_deploy_key:
                method = 'PUT'
                url += '/%s' % fc_key['id']
        if not fc_key:
            method = 'POST'

        if not method:
            return

        _logger.debug('Updating gitlab project deploy keys of %s', repo['path'])
        data = json.dumps(data).encode('utf8')

        req = request.Request(
                url=url,
                data=data,
                method=method,
                headers={
                    'Private-Token': self._private_token,
                    'Content-Type': 'application/json'
                }
        )
        try:
            response = request.urlopen(req)
            if response.code not in [200, 201]:
                _logger.error('Error while creating or updating deploy key for project %s with code %s', repo['name'],
                              response.code)
                return False

        except HTTPError as err:
            result = err.fp.read()
            _logger.error(result)
            return False

    def delete(self, name):
        name = self._rename_repos.get(name, name)
        if name not in self._repos:
            return

        _logger.info('flectra community repository %s exists, deleting it', name)
        repo = self._repos[name]
        if repo['has_master_branch']:
            _logger.warning('flectra community repository %s has master branch, do not delete!', name)
            return

        _logger.debug('Deleting gitlab repository %s', name)
        req = request.Request(
                url='https://gitlab.com/api/v4/projects/%s' % repo['id'],
                method='DELETE',
                headers={
                    'Private-Token': self._private_token,
                }
        )
        response = request.urlopen(req)
        _logger.debug('Deletion with return code %s', response.code)


class LocalRepo(object):

    def __init__(self, repo_path, name, has_fix_branch):
        self._name = name
        self._repo_path = repo_path
        self._repo = git.Repo(self._repo_path)
        self._has_fix_branch = has_fix_branch
        self._author = Actor("Flectra Community Bot", "info@flectra-community.org")

    @property
    def has_fix_branch(self):
        return self._has_fix_branch

    @property
    def has_changes(self):
        return self._repo.is_dirty()

    def add_all(self):
        self._repo.git.add(all=True)

    def commit_changes(self):
        _logger.info('Commiting changes of repository %s', self._name)
        self._repo.index.commit('Automatic Update form OCA2FC Migrator', author=self._author)

    def create_fixed_branch(self):
        self._repo.git.checkout(b='20-fixed')

    def merge_upstream_to_fixed(self):
        self._repo.git.checkout('20-fixed')
        self._repo.git.merge('20-upstream', strategy_option='theirs')

    def push_changes(self):
        _logger.info('Pushing changes of repository %s to gitlab', self._name)
        origin = self._repo.remote()
        origin.push('20-upstream', u=True)
        origin.push('20-fixed', u=True)
